package org.todo.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.todo.entities.Todo;
import org.todo.service.TodoService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("todo")
@RequiredArgsConstructor
public class TodoController {

    private final TodoService service;

    @GetMapping
    public List<Todo> getTodoList() {
        return service.getTodoList();
    }

    @PostMapping
    public Todo saveTodo(@Valid @RequestBody Todo todo) {
        return service.saveTodo(todo);
    }

    @DeleteMapping
    public void deleteTodo(@RequestParam(name = "id", required = false) Integer id) {
        service.deleteTodo(id);
    }

}
