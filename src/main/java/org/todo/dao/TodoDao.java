package org.todo.dao;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.todo.entities.Todo;
import org.todo.service.exceptions.TodoNotFoundException;

import java.util.List;

import static org.springframework.jdbc.core.BeanPropertyRowMapper.newInstance;

@Repository
public class TodoDao {

    private static final BeanPropertyRowMapper<Todo> ROW_MAPPER = newInstance(Todo.class);
    private final JdbcTemplate jdbcTemplate;
    private final SimpleJdbcInsert insertTodo;

    public TodoDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.insertTodo = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("todo")
                .usingColumns("text")
                .usingGeneratedKeyColumns("id");
    }

    public List<Todo> getTodoList() {
        return jdbcTemplate.query("SELECT id, text FROM todo ORDER BY id", ROW_MAPPER);
    }

    public Todo createTodo(Todo todo) {
        BeanPropertySqlParameterSource parameterSource = new BeanPropertySqlParameterSource(todo);
        Number newKey = insertTodo.executeAndReturnKey(parameterSource);
        todo.setId(newKey.intValue());
        return todo;
    }

    public Todo updateTodo(Todo todo) {
        int countUpdated = jdbcTemplate.update("UPDATE todo SET text = ? WHERE id = ?", todo.getText(), todo.getId());
        if (countUpdated == 0) {
            throw new TodoNotFoundException(todo.getId());
        }
        return todo;
    }

    public void deleteTodoList() {
        jdbcTemplate.update("DELETE FROM todo");
    }

    public void deleteTodoById(Integer id) {
        int countDeleted = jdbcTemplate.update("DELETE FROM todo WHERE id = ?", id);
        if (countDeleted == 0) {
            throw new TodoNotFoundException(id);
        }
    }


}
