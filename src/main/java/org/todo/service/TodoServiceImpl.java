package org.todo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.todo.dao.TodoDao;
import org.todo.entities.Todo;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TodoServiceImpl implements TodoService {

    private final TodoDao dao;

    @Override
    public List<Todo> getTodoList() {
        return dao.getTodoList();
    }

    @Override
    public Todo saveTodo(Todo todo) {
        if (todo.getId() == null) {
            return dao.createTodo(todo);
        }
        return dao.updateTodo(todo);
    }

    @Override
    public void deleteTodo(Integer id) {
        if (id == null) {
            dao.deleteTodoList();
            return;
        }
        dao.deleteTodoById(id);
    }
}
