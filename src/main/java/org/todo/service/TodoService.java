package org.todo.service;

import org.todo.entities.Todo;

import java.util.List;

public interface TodoService {

    List<Todo> getTodoList();

    Todo saveTodo(Todo todo);

    void deleteTodo(Integer id);
}
