package org.todo.service.exceptions;

public class BaseApplicationException extends RuntimeException {

    public BaseApplicationException(String message) {
        super(message);
    }

}
