package org.todo.service.exceptions;

public class TodoNotFoundException extends BaseApplicationException {

    public TodoNotFoundException(Integer id) {
        super("Todo by id: " + id + " not found");
    }

}
