drop table IF EXISTS todo;
drop sequence IF EXISTS global_seq;

create sequence global_seq start with 1000;

create TABLE todo
(
    id           INTEGER PRIMARY KEY DEFAULT nextval('global_seq'),
    text         VARCHAR                           NOT NULL
);