package org.todo.suites;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.transaction.annotation.Transactional;
import org.todo.dao.TodoDao;
import org.todo.entities.Todo;

import java.util.function.Consumer;

@SpringBootTest(classes = TestSuiteConfiguration.class)
@AutoConfigureMockMvc
@ContextConfiguration(initializers = Postgres.Initializer.class)
@Transactional
@Sql("/schema.sql")
public abstract class BaseTestSuite {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .registerModule(new ParameterNamesModule());

    @Autowired
    protected MockMvc mockMvc;
    @Autowired
    private TodoDao todoDao;

    protected Todo createTodo() {
        return createTodo(userEntity -> {
        });
    }

    protected Todo createTodo(Consumer<Todo> builder) {
        Todo todo = new Todo();
        todo.setText("Test text");
        builder.accept(todo);
        return todoDao.createTodo(todo);
    }

    public String asJson(Object object) throws Exception {
        return OBJECT_MAPPER
                .writer()
                .withDefaultPrettyPrinter()
                .writeValueAsString(object);
    }

    public <T> T fromResponse(MvcResult result, Class<T> type) throws Exception {
        return OBJECT_MAPPER
                .readerFor(type)
                .readValue(result.getResponse().getContentAsString());
    }

    public <T> T fromJson(String json, Class<T> type) throws Exception {
        return OBJECT_MAPPER
                .readerFor(type)
                .readValue(json);
    }
}
