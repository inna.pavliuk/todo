package org.todo.suites;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.todo.Runner;

@Configuration
@Import(Runner.class)
public class TestSuiteConfiguration {
}
