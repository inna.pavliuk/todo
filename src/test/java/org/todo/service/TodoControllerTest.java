package org.todo.service;

import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import org.todo.entities.Todo;
import org.todo.suites.BaseTestSuite;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class TodoControllerTest extends BaseTestSuite {

    public static final String TODO = "/todo";
    private Integer TODO_ID;


    @Test
    void todoListIsEmptyWhenNothingCreatedTest() throws Exception {
        mockMvc.perform(get(TODO).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    void getTodoListTest() throws Exception {
        todoListIsEmptyWhenNothingCreatedTest();

        Todo todo = createTodo();
        TODO_ID = todo.getId();

        MvcResult resultAfterSave = mockMvc.perform(get(TODO).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Todo[] allTodoAfterSave = fromResponse(resultAfterSave, Todo[].class);
        assertThat(allTodoAfterSave).hasSize(1);

        Todo oneTodo = allTodoAfterSave[0];
        assertThat(oneTodo.getId()).isEqualTo(TODO_ID);
        assertThat(oneTodo.getText()).isEqualTo(todo.getText());
    }

    @Test
    void createTodoTest() throws Exception {
        todoListIsEmptyWhenNothingCreatedTest();

        Todo request = new Todo(null, "Test text");
        MvcResult result = mockMvc.perform(post(TODO).content(asJson(request))
                        .contentType(APPLICATION_JSON).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Todo todoFromResponse = fromResponse(result, Todo.class);
        assertThat(todoFromResponse.getId()).isNotNull();
        assertThat(todoFromResponse.getText()).isEqualTo(request.getText());

        TODO_ID = todoFromResponse.getId();
        MvcResult resultAfterSave = mockMvc.perform(get(TODO).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Todo[] allTodoAfterSave = fromResponse(resultAfterSave, Todo[].class);
        assertThat(allTodoAfterSave).hasSize(1);

        Todo oneTodo = allTodoAfterSave[0];
        assertThat(oneTodo.getId()).isEqualTo(TODO_ID);
        assertThat(oneTodo.getText()).isEqualTo(request.getText());

    }

    @Test
    void updateTodoTest() throws Exception {
        todoListIsEmptyWhenNothingCreatedTest();
        Todo todo = createTodo();
        TODO_ID = todo.getId();

        Todo request = new Todo(TODO_ID, "New text");
        MvcResult result = mockMvc.perform(post(TODO).content(asJson(request))
                        .contentType(APPLICATION_JSON).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Todo todoFromResponse = fromResponse(result, Todo.class);
        assertThat(todoFromResponse.getId()).isEqualTo(TODO_ID);
        assertThat(todoFromResponse.getText()).isEqualTo(request.getText());

        MvcResult resultAfterUpdate = mockMvc.perform(get(TODO).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        Todo[] allTodoAfterUpdate = fromResponse(resultAfterUpdate, Todo[].class);
        assertThat(allTodoAfterUpdate).hasSize(1);

        Todo oneTodo = allTodoAfterUpdate[0];
        assertThat(oneTodo.getId()).isEqualTo(TODO_ID);
        assertThat(oneTodo.getText()).isEqualTo(request.getText());

    }

    @Test
    void deleteAllTodoTest() throws Exception {
        todoListIsEmptyWhenNothingCreatedTest();
        createTodo();

        mockMvc.perform(delete(TODO))
                .andExpect(status().isOk());

        mockMvc.perform(get(TODO).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    void deleteTodoByIdTest() throws Exception {
        todoListIsEmptyWhenNothingCreatedTest();
        Todo todo = createTodo();

        mockMvc.perform(delete(TODO + "?id=" + todo.getId())
                        .accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        mockMvc.perform(get(TODO).accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }
}